(define-package "live-py-mode" "20170225.2207" "Live Coding in Python"
  '((emacs "24.1"))
  :url "http://donkirkby.github.io/live-py-plugin/" :keywords
  '("live" "coding"))
;; Local Variables:
;; no-byte-compile: t
;; End:
